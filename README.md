# Projet Gestion de Budget
---


# L'interface graphique :

L'interface graphique est composée :

D'un bloc formulaire recevant en description, les entées, les valeurs, la date de l'alfux géré par l'utilisateur.
D'une seconde partie contenant les informations relatives au compte (solde, rentrer, sortie), qui  évolue en fonction des information saisies pas l'utilisateur.
Puis pour terminer d'un troisième bloc qui montre les opérations éffectuées.

# Maquette :
---

![Maquette](Maquette.jpg)

réalisée avec le logiciel web Figma.

# La Structure du Projet :

Le projet contient :

* Un fichier index.html et un fichier style.css qui contient toute l'interface de l'application.
* Un fichier main.ts(un peu bordelique ^^') qui contient tout les script de l'application.
* Puis tous les autres fichier qui ont été automatiquement généré par Vite.

# Fonctionnement de l'application :

Les premières lignes du fichier main.js sert surtout à récupérer les différents éléments du DOM et à déclarer les variables et les listes qui auront leur utilité dans plusieurs fonctions.
La première propriété, est "localstoragetransactions". Son rôle est, comme son nom l'indique, d'obtenir des transactions à partir du stockage local. Puis avec l'aide d'une seconde propriété appelée 'transactions' elle vien crée un tableau de string, pour ensuite obtenir un resultat.

Les secondes lignes sont constituées de plusieurs fonctions comme par Exemple 'addTransaction' qui comme son nom l'indique d'ajouter une transaction, ou bien 'generateID" qui a pour but de générer random ID mais encore 'updateValues" qui elle va mettre à jour le solde, les entrées et les sorties.
Nous avons aussi une fonction "removeTransaction" qui va permettre de supprimer les transactions inscrites

Par exemple si nous prenons la fonctions 'removeTransaction'

```

function removeTransaction(id: number) {
  transactions = transactions.filter((transaction: { id: number; }) => transaction.id !== id);

  updateLocalStorage();

  start();
}


```

elle sera géré par un type 'Opperation' comme pour la plupart de ces fonctions' 

```
type Operation = {
  id: number;
  text: string;
  amount: number;
  date: string;
};

```

Ce qui nous permettra à presque toute d'entre elles, d'assigner une valeur qu'elle soit nulle ou non. 


Enfin pour la derniére partie le but etant d'ajouter une classe en fonction de la valeur" et "d'obetenir un signe" a l'aide d'une simple propriété. 
Celle ci comprend aussi la 'date' qui et fixé dans le HTML, mais est représentée par une propriété dans la main ts, Idem pout le "texte", le "solde" grace à la propriété "item" directement reliée au HTML a l'aide d'un '.InnerHTML', grâce a celui-ci l'utilisateur va pouvoir modifier ses valeur en fonction de ses besoins.


```

// Historique de transactions
function addTransactionDOM(transaction: Operation) {
  // Obtenir un signe
  const sign = transaction.amount < 0 ? "-" : "+";

  const item = document.createElement("li");

  // Ajouter une classe en fonction de la valeur
  item.classList.add(transaction.amount < 0 ? "moins" : "plus");

  item.innerHTML = `
    ${transaction.text}
    ${transaction.date}
    ${sign}
    ${Math.abs(transaction.amount)}
     <button class="delete-btn">X</button>
  `;
  item.querySelector("button")?.addEventListener("click", () => removeTransaction(transaction.id));

  list!.appendChild(item);
}

```

