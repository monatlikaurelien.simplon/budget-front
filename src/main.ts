import './style.css'

const balance = document.querySelector<HTMLParagraphElement>('#balance');
const inflow = document.querySelector<HTMLParagraphElement>('#income');
const outflow = document.querySelector<HTMLParagraphElement>('#expense');
const list = document.querySelector<HTMLUListElement>('#list');
const form = document.querySelector<HTMLFormElement>('#form');
const text = document.querySelector<HTMLInputElement>('#text');
const amount = document.querySelector<HTMLInputElement>('#amount');
const date = document.querySelector<HTMLInputElement>('#date');


// Obtenir des transactions à partir du stockage local
const localStorageTransactions = JSON.parse(
  localStorage.getItem("transactions")!
);

let transactions =
  localStorage.getItem("transactions") !== null ? localStorageTransactions : [];

// Ajouter une transaction
function addTransaction(e: SubmitEvent) {
  e.preventDefault();
  if (!text?.value || !amount?.value || !date?.value) {
    return;
  }
  if (text.value.trim() === "" || amount.value.trim() === "") {
    document.getElementById("error_msg")!.innerHTML =
      "<span >Erreur : Veuillez saisir la description et le montant !</span>";
    setTimeout(
      () => (document.getElementById("error_msg")!.innerHTML = ""),
      5000
    );
  } else {
    const transaction: Operation = {
      id: generateID(),
      text: text.value,
      amount: +amount.value,
      date: date.value
    };

    transactions.push(transaction);

    addTransactionDOM(transaction);

    updateValues();

    updateLocalStorage();

    text.value = "";
    amount.value = "";
  }
}

// Generer random ID
function generateID() {
  return Math.floor(Math.random() * 100000000);
}

type Operation = {
  id: number;
  text: string;
  amount: number;
  date: string;
};

// Historique de transactions
function addTransactionDOM(transaction: Operation) {
  // Obtenir un signe
  const sign = transaction.amount < 0 ? "-" : "+";

  const item = document.createElement("li");

  // Ajouter une classe en fonction de la valeur
  item.classList.add(transaction.amount < 0 ? "moins" : "plus");

  item.innerHTML = `
    ${transaction.text}
    ${transaction.date}
    ${sign}
    ${Math.abs(transaction.amount)}
     <button class="delete-btn">X</button>
  `;
  item.querySelector("button")?.addEventListener("click", () => removeTransaction(transaction.id));

  list!.appendChild(item);
}

// Mettre à jour le solde, les entrées et les sorties
function updateValues() {
  const amounts = transactions.map((transaction: { amount: number; }) => transaction.amount);

  const total = amounts.reduce((bal: number, value: number) => (bal += value), 0).toFixed(2);

  const income = amounts
    .filter((value: number) => value > 0)
    .reduce((bal: number, value: number) => (bal += value), 0)
    .toFixed(2);

  const expense =
    amounts
      .filter((value: number) => value < 0)
      .reduce((bal: number, value: number) => (bal += value), 0) * -(1).toFixed(2);

  balance!.innerText = '€' + total;
  inflow!.innerText = '€' + income;
  outflow!.innerText = '€' + expense;
}

// Supprimer la transaction par ID
function removeTransaction(id: number) {
  transactions = transactions.filter((transaction: { id: number; }) => transaction.id !== id);

  updateLocalStorage();

  start();
}

// Mettre à jour les transactions de stockage local
function updateLocalStorage() {
  localStorage.setItem("transactions", JSON.stringify(transactions));
}

// Start app
function start() {
  list!.innerHTML = "";
  transactions.forEach(addTransactionDOM);
  updateValues();
}

start();

form?.addEventListener("submit", addTransaction);